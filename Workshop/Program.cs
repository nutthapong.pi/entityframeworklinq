﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop
{
    class Program
    {
        static void Main(string[] args)
        {
            //WS1();
            //WS2();
            //WS3();
            //WS4();
        }

        static private void WS1()
        {
            using (var db = new WShopEntities1())
            {
                string str = "US";

                var ds = (from a in db.CUSTOMER
                          join b in db.COUNTRY on a.COUNTRY_CODE equals b.COUNTRY_CODE
                          where b.COUNTRY_CODE.Contains(str)
                          select new
                          {
                              CusID = a.CUSTOMER_ID,
                              CusName = a.NAME,
                              CusEmail = a.EMAIL,
                              CountyName = b.COUNTRY_NAME,
                              Budget = a.BUDGET,
                              User = a.USED
                          }).ToList();

                if (ds.Count > 0)
                {
                    foreach (var item in ds)
                    {
                        Console.WriteLine(string.Format("{0} | {1} | {2} | {3} | {4} | {5}"
                            , item.CusID
                            , item.CusName
                            , item.CusEmail
                            , item.CountyName
                            , item.Budget
                            , item.User));
                    }
                }
                Console.ReadLine();
            }
        }

        static private void WS2()
        {
            using (var db = new WShopEntities1())
            {
                db.CUSTOMER.Add(new CUSTOMER()
                {
                    CUSTOMER_ID = "C006",
                    NAME = "Nutthapong",
                    EMAIL = "nutthapong.pi@ku.th",
                    COUNTRY_CODE = "TH",
                    BUDGET = 5000000,
                    USED = 0,

                });

                db.SaveChanges();
                Console.WriteLine("----add data success----");
            }
            Console.ReadLine();
        }

        static private void WS3()
        {
            using (var db = new WShopEntities1()) 
            {
                string name = "J";

                // Update Statement
                var update = db.CUSTOMER.Where(a => a.NAME.Contains(name)).ToList();
                if (update != null)
                {
                    update.ForEach(a =>
                    {
                        a.BUDGET = 0;
                        a.USED = 1;
                    });
                    
                }

                db.SaveChanges();
                Console.WriteLine("----update data success----");
                Console.ReadLine();
            }
        }

        static private void WS4()
        {
            string idlast = null;
            using (var db = new WShopEntities1())
            {
                

                var del = db.CUSTOMER.ToList();
                if (del != null)
                {
                    del.ForEach(a =>
                    {
                        idlast = a.CUSTOMER_ID;
                    });

                    
                }

                var deldata = db.CUSTOMER.Where(a => a.CUSTOMER_ID == idlast).FirstOrDefault();
                if(deldata != null)
                {
                    db.CUSTOMER.Remove(deldata);
                }



                db.SaveChanges();
                Console.WriteLine("----delete data success----");
                Console.WriteLine(idlast);

            }
            Console.ReadLine();
        }
    }
}
